package com.godel;

public class BubbleSortBus implements IBusinessInterface{

	public boolean PreExecute(EXECUTION_CONTEXT context) {
		if(context == null){
			return false;
		}
		return true;
	}

	public boolean Execute(EXECUTION_CONTEXT context) {
		
		double[] arr = bubbleSort((double[])context.getCargo("DATA"));
		context.setCargo("RESULT", arr);
		return true;
	}

	public boolean PostExecute(EXECUTION_CONTEXT context) {
		return true;
	}
	
	private double[] bubbleSort(double[] arr) {
		int length = arr.length;
	    for(int i = 0;i < length; i++){
	        for(int j= i; j < length; j++){
	            if(arr[i] > arr[j]){
	                double temp = arr[i];
	                arr[i] = arr[j];
	                arr[j] = temp;
	            }
	        }
	    }
	    for (int i = 0;i < length; i++) {
	        System.out.println(arr[i]);
	    }
	    
	    return arr;
	}

}
