package com.godel;

public interface IBusinessInterface {
	
	boolean PreExecute(EXECUTION_CONTEXT context);
	boolean Execute(EXECUTION_CONTEXT context);
	boolean PostExecute(EXECUTION_CONTEXT context);

}
