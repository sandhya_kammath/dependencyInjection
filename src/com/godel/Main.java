package com.godel;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Main {
	
	public static void main(String[] args){
		System.out.println("Inside Main");
		
		int length = args.length;
		if(length < 2){
			System.out.println("Incorrect Sysntax");
			return;
		}
		String cmd = args[0];	
		System.out.println(cmd);
		double [] arr = new double[length];
		for(int i = 1; i < length;i++) {
			arr[i-1] = Double.parseDouble(args[i]);
		}
		ISorter sorter = null;
		/*if(cmd.equalsIgnoreCase("ASC_SORT")){
			sorter = new BubbleSorter();
			
		}else if(cmd.equalsIgnoreCase("DESC_SORT")){
			sorter = new DecendingOrderSorter();
		}
		else{
			System.out.println("InvalidSort option");
			return;
		}*/
		
		//sort(arr);
		//ISorter sorter = new BubbleSorter();
		//sorter.execute(arr);
		
		/*sorter = (ISorter)SortFactory.get(cmd);
		if(sorter !=null){
			sorter.execute(arr);
		}
		else{
			System.out.println("InvalidSort option");
			return;
		}*/
		
		double[] res = null;
		IBusinessInterface bus = null;
		try {
			try {
				bus = (IBusinessInterface)PluginManager.get(cmd);
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		EXECUTION_CONTEXT context = new EXECUTION_CONTEXT();
		context.setCargo("DATA",arr);
		if(bus.PreExecute(context)){
			if(bus.Execute(context)){
				bus.PostExecute(context);
				res = (double[]) context.getCargo("RESULT");
			}
		}
		for (int i = 0;i < res.length; i++) {
	        System.out.println(res[i]);
	    }
		
	}

	

	private static void sort(double[] arr) {
		int length = arr.length;
	    for(int i = 0;i < length; i++){
	        for(int j= i; j < length; j++){
	            if(arr[i] > arr[j]){
	                double temp = arr[i];
	                arr[i] = arr[j];
	                arr[j] = temp;
	            }
	        }
	    }
	    for (int i = 0;i < length; i++) {
	        System.out.println(arr[i]);
	    }
	}
	
	

}
