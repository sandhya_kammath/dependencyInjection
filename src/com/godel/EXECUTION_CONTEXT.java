package com.godel;
import java.util.HashMap;
import java.util.Map;


public class EXECUTION_CONTEXT {
	
	private Map<String,Object> cargo = new HashMap<String, Object>();;

	public Object getCargo(String key) {
		return cargo.get(key);
	}

	public void setCargo(String key, Object value) {
		cargo.put(key, value); 
	}

}
