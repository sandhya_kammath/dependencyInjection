package com.godel;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PluginManager {
	
	static Map<String,String> map = new HashMap<String, String>();
	
	static {
		File file = new File("/Users/Godel/SANDHYA_FILES/STUDY/JAVA/Sort/src/Sort.xml");
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document doc= null;
		try {
			doc = builder.parse(file);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doc.getDocumentElement().normalize();
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		NodeList nList = doc.getElementsByTagName("plugin");
		System.out.println("\nCurrent Element :" + nList.getLength());
		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);
					
			System.out.println("\nCurrent Element :" + nNode.getNodeName());
					
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) nNode;
				
				map.put(eElement.getAttribute("key"),eElement.getAttribute("value"));			

			}
		}
	}
	
	public static Object get(String name) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
		IBusinessInterface bus = null;
		/*if(cmd.equalsIgnoreCase("ASC_SORT")){
			bus = new BubbleSortBus();
			
		}else if(cmd.equalsIgnoreCase("DESC_SORT")){
			bus = new DescendingSortBus();
		}*/
		Object obj = null;
		Class<?> cls = Class.forName(map.get(name));
		 bus = (IBusinessInterface)cls.newInstance();
	
		return bus;
	}


}
