package com.godel;

public interface ISorter {
	boolean execute(double[] arr);

}
