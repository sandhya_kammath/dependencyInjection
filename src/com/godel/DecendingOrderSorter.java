package com.godel;

public class DecendingOrderSorter implements ISorter{
	public boolean execute(double[] arr) {
		int length = arr.length;
	    for(int i = 0;i < length; i++){
	        for(int j= i; j < length; j++){
	            if(arr[i] < arr[j]){
	                double temp = arr[i];
	                arr[i] = arr[j];
	                arr[j] = temp;
	            }
	        }
	    }
	    for (int i = 0;i < length; i++) {
	        System.out.println(arr[i]);
	    }
	    return true;
	}

}
