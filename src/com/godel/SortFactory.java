package com.godel;

public class SortFactory {
	
	public static Object get(String cmd){
		ISorter sorter = null;
		if(cmd.equalsIgnoreCase("ASC_SORT")){
			sorter = new BubbleSorter();
			
		}else if(cmd.equalsIgnoreCase("DESC_SORT")){
			sorter = new DecendingOrderSorter();
		}
		
		return sorter;
	}

}
